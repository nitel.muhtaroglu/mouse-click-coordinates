package cs102;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MyMouseListener extends MouseAdapter {
    private JLabel label;

    public MyMouseListener(JLabel label) {
        this.label = label;
    }

    public void mouseClicked(MouseEvent mouseEvent) {
        System.out.println("Clicked at " + mouseEvent.getX() + ", " + mouseEvent.getY());
        System.out.println(" on mouse button " + mouseEvent.getButton());
        System.out.println(" for " + mouseEvent.getClickCount() + " time(s).");
    }

    public void mousePressed(MouseEvent mouseEvent) {
        this.label.setText(mouseEvent.getX() + ", " + mouseEvent.getY());
    }
}
